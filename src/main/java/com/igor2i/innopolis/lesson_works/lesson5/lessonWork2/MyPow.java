package com.igor2i.innopolis.lesson_works.lesson5.lessonWork2;

/**
 * Created by igor2i on 10.02.17.
 */
public class MyPow implements Runnable {

    private int pow = 1;
    private int[] mass;
    private Cunsumer cunsumer;

    MyPow(int pow, int[] mass, Cunsumer cunsumer) {
        this.pow = pow;
        this.mass = mass;
        this.cunsumer = cunsumer;
    }

    @Override
    public void run() {

        int outSumm = 0;
        for (int mas : mass) {
            outSumm += Math.pow(mas, pow);
        }

        cunsumer.message(outSumm);
    }
}
