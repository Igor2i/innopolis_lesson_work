package com.igor2i.innopolis.lesson_works.lesson5.lessonWork2_2;

/**
 * Created by igor2i on 11.02.17.
 */
public enum FieldType {
    INT {
        public Object parse(String var) {
            return Integer.valueOf(var);
        }
    },
    STRING {
        public Object parse(String var) {
            return var;
        }
    },
    DOUBLE {
        public Object parse(String var) {
            return Double.valueOf(var);
        }
    },
    SHORT {
        public Object parse(String var) {
            return Short.valueOf(var);
        }
    },
    BYTE {
        public Object parse(String var) {
            return Byte.valueOf(var);
        }
    },
    LONG {
        public Object parse(String var) {
            return Long.valueOf(var);
        }
    },
    FLOAT {
        public Object parse(String var) {
            return Float.valueOf(var);
        }
    },
    CHAR {
        public Object parse(String var) {
            return var.charAt(0);
        }
    },
    BOOLEAN{
        public Object parse(String var){
            return Boolean.valueOf(var);
        }
    };



    public static Object parse(String type, String var) throws IllegalArgumentException {

        switch (type) {
            case "java.lang.String": return var;
            case "int": return INT.parse(var);
            case "java.lang.Integer": return INT.parse(var);
            case "short": return SHORT.parse(var);
            case "java.lang.Short" : return SHORT.parse(var);
            case "byte": return BYTE.parse(var);
            case "java.lang.Byte" : return BYTE.parse(var);
            case "long": return LONG.parse(var);
            case "java.lang.Long" : return LONG.parse(var);
            case "double": return DOUBLE.parse(var);
            case "java.lang.Double" : return DOUBLE.parse(var);
            case "float": return FLOAT.parse(var);
            case "java.lang.Float" : return FLOAT.parse(var);
            case "char": return CHAR.parse(var);
            case "java.lang.Character" : return CHAR.parse(var);
            case "boolean": return BOOLEAN.parse(var);
            default:
                throw new IllegalArgumentException("Type not supported");
        }
    };

    public abstract Object parse(String var);

}
