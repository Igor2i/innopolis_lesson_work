package com.igor2i.innopolis.lesson_works.lesson5.lessonWork2_2;

import org.apache.commons.io.FileUtils;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;
import java.net.URL;

/**
 * Created by igor2i on 10.02.17.
 */
public class Main {

    public static void main(String[] args) {

        People people = new People();
        people.setName("Jone");
        people.setSalary(10000.0);
        people.setAge(25);


        SerializationIntoXML serializationIntoXML = new SerializationIntoXML(people);
        String objSerializ = "";
        try {
            Document document = serializationIntoXML.getSerializObject();
            objSerializ = serializationIntoXML.printDocumentAsXML(document);
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println("Создан XML \r\n" + objSerializ);
        System.out.println();


        DeserializationIntoObject deserializationIntoObject = new DeserializationIntoObject(objSerializ);
        try {

            Object objectDeserializ = deserializationIntoObject.getDeserializObject();
            System.out.println("XML преобразован в обьект \r\n" + objectDeserializ.toString());

        } catch (ParserConfigurationException | IOException | SAXException | ClassNotFoundException | IllegalAccessException | InstantiationException | NoSuchFieldException e) {
            e.printStackTrace();
        }


        System.out.println();
        System.out.println("--------------------------------");
        System.out.println();


        /**
         * ClassLoader  - загружает Jar файл с классом Animal
         */

        try {
            System.out.println("Получаем класс из jar файла");
            URL url = new URL("https://bitbucket.org/Igor2i/innopolis_lesson_work/raw/e3f8eb43761e9cf0640fa71a6eef416ddd2622c9/animal.jar");
            File fileJar = new File("testAnimal.jar");
            FileUtils.copyURLToFile(url, fileJar);

            JarClassLoader jarClassLoader = new JarClassLoader(fileJar);
            Class aClass = jarClassLoader.loadClass("Animal");

            Object animal = aClass.newInstance();

            System.out.println(animal.toString());

            SerializationIntoXML serializationIntoXML1 = new SerializationIntoXML(animal);
            String objS = "";
            try {
                objS = serializationIntoXML1.printDocumentAsXML(serializationIntoXML1.getSerializObject());
            } catch (Exception e) {
                e.printStackTrace();
            }
            System.out.println("Создан XML \r\n" + objS);
        System.out.println();

        } catch (ClassNotFoundException | IllegalAccessException | InstantiationException | IOException e) {
            e.printStackTrace();
        }


//        Animal animal = new Animal();
//        animal.setName("Барсик");
//        animal.setAge(5);
//        animal.setFooters(4);
//        animal.setHair(true);
//        animal.setMale(true);
//
//        SerializationIntoXML serializationIntoXML1 = new SerializationIntoXML(animal);
//        String objS = "";
//        try {
//            objS = serializationIntoXML1.printDocumentAsXML(serializationIntoXML1.getSerializObject());
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        System.out.println("Создан XML \r\n" + objS);
//        System.out.println();
//
//        DeserializationIntoObject deserializationIntoObject1 = new DeserializationIntoObject(objS);
//        try {
//            Object objDesA = deserializationIntoObject1.getDeserializObject();
//            System.out.println("XML преобразован в обьект \r\n" + objDesA.toString());
//
//        } catch (ParserConfigurationException | IOException | SAXException | ClassNotFoundException | IllegalAccessException | InstantiationException | NoSuchFieldException e) {
//            e.printStackTrace();
//        }


    }

}
