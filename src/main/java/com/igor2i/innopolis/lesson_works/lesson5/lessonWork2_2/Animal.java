////package com.igor2i.innopolis.test_project.lesson5.lessonWork2_2;
//
///**
// * Created by igor2i on 11.02.17.
// */
//public class Animal {
//
//    public static final long SerialVersionUIDAdder = 1L;
//
//    private String name;
//    private int age;
//    private boolean hair;
//    private boolean male;
//    private int footers;
//
//    public Animal(String name, int age, boolean hair, boolean male, int footers) {
//        this.name = name;
//        this.age = age;
//        this.hair = hair;
//        this.male = male;
//        this.footers = footers;
//    }
//
//    public Animal() {
//    }
//
//    @Override
//    public String toString() {
//        return "Animal{" +
//                "name='" + name + '\'' +
//                ", age=" + age +
//                ", hair=" + hair +
//                ", male=" + male +
//                ", footers=" + footers +
//                '}';
//    }
//
//    public String getName() {
//        return name;
//    }
//
//    public void setName(String name) {
//        this.name = name;
//    }
//
//    public int getAge() {
//        return age;
//    }
//
//    public void setAge(int age) {
//        this.age = age;
//    }
//
//    public boolean isHair() {
//        return hair;
//    }
//
//    public void setHair(boolean hair) {
//        this.hair = hair;
//    }
//
//    public boolean isMale() {
//        return male;
//    }
//
//    public void setMale(boolean male) {
//        this.male = male;
//    }
//
//    public int getFooters() {
//        return footers;
//    }
//
//    public void setFooters(int footers) {
//        this.footers = footers;
//    }
//}
