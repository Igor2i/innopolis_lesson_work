package com.igor2i.innopolis.lesson_works.lesson5.lessonWork2;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by igor2i on 10.02.17.
 */
public class Cunsumer {

    private AtomicInteger summ = new AtomicInteger();

    void message(int inSumm){
        System.out.println( "Сейчас sum = " + summ.addAndGet( inSumm) );
    }
}
