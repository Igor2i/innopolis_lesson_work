package com.igor2i.innopolis.lesson_works.lesson5.lessonWork2_2;


/**
 * Created by igor2i on 10.02.17.
 */
public class People {

    public static final long SerialVersionUIDAdder = 12L;

    private String name;
    private Integer age;
    private double salary;

    public People(String name, Integer age, double salary) {
        this.name = name;
        this.age = age;
        this.salary = salary;
    }

    public People() {
    }

    protected void paySalary(){
        System.out.println("I have ");
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public double getSalary() {
        return salary;
    }

    public void setSalary(double salary) {
        this.salary = salary;
    }

    @Override
    public String toString() {
        return "People{" +
                "name='" + name + '\'' +
                ", age=" + age +
                ", salary=" + salary +
                '}';
    }
}
