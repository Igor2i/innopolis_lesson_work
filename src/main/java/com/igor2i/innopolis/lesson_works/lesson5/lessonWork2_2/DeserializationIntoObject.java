package com.igor2i.innopolis.lesson_works.lesson5.lessonWork2_2;

import org.w3c.dom.*;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.io.StringReader;
import java.lang.reflect.Field;
import java.util.IllegalFormatFlagsException;

/**
 * Created by igor2i on 10.02.17.
 */
public class DeserializationIntoObject {

    private String xml;

    public DeserializationIntoObject(String xml) {
        this.xml = xml;
    }



    public Object getDeserializObject() throws ParserConfigurationException, IOException, SAXException, ClassNotFoundException, IllegalAccessException, InstantiationException, NoSuchFieldException {

        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        dbf.setNamespaceAware(true);
        Document document = dbf.newDocumentBuilder().parse(new InputSource(new StringReader(xml)));

        String nameClass = document.getChildNodes().item(0).getNodeName();

        Class myClass = Class.forName(nameClass);

//        System.out.println("Class - " + myClass.getName());
        Object deserializObject = myClass.newInstance();


        long myVersionObject = 0L;
        myVersionObject = (long)deserializObject.getClass().getField("SerialVersionUIDAdder").get(deserializObject);


        String versionGetObject = document.getElementsByTagName("SerialVersionUIDAdder").item(0).getTextContent();

        if( Long.parseLong(versionGetObject) == myVersionObject){

//            System.out.println("Object version is equally");

            NodeList fields = document.getElementsByTagName("Fields").item(0).getChildNodes();
            for(int i = 0; i < fields.getLength() ; i++ ){
                Node childNode = fields.item(i);

                if(childNode.hasAttributes() && !( "SerialVersionUIDAdder".equals(childNode.getNodeName()))) {

                    //field
                    String fieldName = childNode.getNodeName();
                    //type
                    String fieldType = childNode.getAttributes().item(0).getNodeValue();
                    //value
                    String value = childNode.getTextContent();

                    Field useField = deserializObject.getClass().getDeclaredField(fieldName);
                    useField.setAccessible(true);
                    useField.set(deserializObject, FieldType.parse(fieldType, value) );

                }
            }
        }else{
            throw new IllegalFormatFlagsException("My "+ deserializObject.getClass().getSimpleName() +" version " + myVersionObject + " isn't equal this Object");
        }


        return deserializObject;

    }

}
