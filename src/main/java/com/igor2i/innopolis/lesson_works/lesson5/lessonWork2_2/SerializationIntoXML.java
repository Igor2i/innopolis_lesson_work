package com.igor2i.innopolis.lesson_works.lesson5.lessonWork2_2;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.StringWriter;
import java.io.Writer;
import java.lang.reflect.Field;
import java.lang.reflect.Method;

/**
 * Created by igor2i on 10.02.17.
 */
public class SerializationIntoXML {

    private Object object;

    public SerializationIntoXML(Object object) {
        this.object = object;
    }

    public Document getSerializObject() throws Exception {
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = dbf.newDocumentBuilder();
        Document doc = builder.newDocument();


        Element element = doc.createElement(object.getClass().getName());
        doc.appendChild(element);

        Element fieldsObject = doc.createElement("Fields");
        element.appendChild(fieldsObject);

        for (Field field : object.getClass().getDeclaredFields()) {

            Element itemElement = doc.createElement(field.getName());
            fieldsObject.appendChild(itemElement);

            field.setAccessible(true);

            itemElement.setAttribute("type", field.getType().getName());
            itemElement.insertBefore(doc.createTextNode(String.valueOf(field.get(object))), itemElement.getLastChild());

        }

        Element methodsObject = doc.createElement("Methods");
        element.appendChild(methodsObject);

        for (Method method : object.getClass().getDeclaredMethods()) {

            Element itemMethod = doc.createElement(method.getName());
            methodsObject.appendChild(itemMethod);

            method.setAccessible(true);

            itemMethod.setAttribute("return", method.getReturnType().getName() );
            itemMethod.setAttribute("return", method.getReturnType().getName() );
            Class[] paramTypes = method.getParameterTypes();
            for (Class paramType : paramTypes) {
                itemMethod.setAttribute("paramType", paramType.getName() );
            }

        }

        return doc;
    }


    public String printDocumentAsXML(Document document) throws Exception {
        Transformer tf = TransformerFactory.newInstance().newTransformer();
        tf.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
        tf.setOutputProperty(OutputKeys.INDENT, "yes");
        Writer out = new StringWriter();
        tf.transform(new DOMSource(document), new StreamResult(out));
        return out.toString();
    }

}
