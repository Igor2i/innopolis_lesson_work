package com.igor2i.innopolis.lesson_works.lesson4.lessonWork;

import com.igor2i.innopolis.lesson_works.lesson4.lessonWork.waiter.WaiterEvent;

/**
 * Created by igor2i on 09.02.17.
 */
public class LessonWork {

    private static final int TIME_SECOND = 1000;
    private static final int TACT_EVENT_T1 = 5;
    private static final int TACT_EVENT_T2 = 7;

    private static final int TIME_SECOND_SHUTDOWN = 50;

    private static final Object lock = new Object();
    private static volatile boolean stop = false;
    private static volatile long second = 0;

    public static void main(String[] args) {

        Thread thread1 = new Thread(new WaiterEvent(TACT_EVENT_T1, lock, "1"));
        Thread thread2 = new Thread(new WaiterEvent(TACT_EVENT_T2, lock, "2"));

        long timeStart = System.currentTimeMillis();

        System.out.println("Start threads" );

        thread1.start();
        thread2.start();

        while (!isStop()) {

            try {
                Thread.sleep(TIME_SECOND);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            second = (System.currentTimeMillis() - timeStart) / TIME_SECOND;
            System.out.println(second + " second lost" );


            synchronized (lock){
                lock.notifyAll();
            }

            if(second == TIME_SECOND_SHUTDOWN){
                setStop();
            }
        }

    }

    public static boolean isStop(){
        return stop;
    }

    private static void setStop(){
        stop = true;
    }

    public static long whatSecond(){
        return second;
    }
}
