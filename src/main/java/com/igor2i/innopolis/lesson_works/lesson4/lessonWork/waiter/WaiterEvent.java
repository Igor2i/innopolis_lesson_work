package com.igor2i.innopolis.lesson_works.lesson4.lessonWork.waiter;

import com.igor2i.innopolis.lesson_works.lesson4.lessonWork.LessonWork;
import static com.igor2i.innopolis.lesson_works.lesson4.lessonWork.LessonWork.whatSecond;

/**
 * Created by igor2i on 09.02.17.
 */
public class WaiterEvent implements Runnable {

    private int tactCount = 0;
    private String name = "undef";
    private Object lock = new Object();

    public WaiterEvent(int tact, Object lock,String name) {
        this.tactCount = tact;
        this.name = name;
        this.lock = lock;
    }

    @Override
    public void run() {

            while (!LessonWork.isStop()){
                synchronized (lock){
                    try {
                        lock.wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                if( whatSecond()  % tactCount == 0  ){
                    System.out.println("Hello, I'm thread " + name);
                }


            }
    }
}
