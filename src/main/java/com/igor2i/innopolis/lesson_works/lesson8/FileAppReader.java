package com.igor2i.innopolis.lesson_works.lesson8;

import java.io.FileReader;
import java.io.IOException;

/**
 * Created by igor2i on 15.02.17.
 */
public class FileAppReader {

    private String fileName;

    public FileAppReader(String fileName) {
        this.fileName = fileName;
    }

    public String readWile(){

        String out = "";
        try(FileReader reader = new FileReader(fileName))
        {
            int c;
            while((c=reader.read())!=-1){

                out += (char)c;
            }
        }
        catch(IOException ex){

            System.out.println(ex.getMessage());
        }

        return out;

    }
}
