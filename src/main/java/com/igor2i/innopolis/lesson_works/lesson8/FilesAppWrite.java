package com.igor2i.innopolis.lesson_works.lesson8;

import java.io.FileWriter;
import java.io.IOException;

/**
 * Created by igor2i on 15.02.17.
 */
public class FilesAppWrite {

    private String fileName;

    public FilesAppWrite(String fileName) {
        this.fileName = fileName;
    }


    public void newFile(String text){

        try(FileWriter writer = new FileWriter(fileName, false))
        {
            writer.write(text);

            writer.flush();
        }
        catch(IOException ex){

            System.out.println(ex.getMessage());
        }

    }


}
