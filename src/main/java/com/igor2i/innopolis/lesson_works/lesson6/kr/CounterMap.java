package com.igor2i.innopolis.lesson_works.lesson6.kr;

import java.util.HashMap;
import java.util.Queue;

import static com.igor2i.innopolis.lesson_works.lesson6.kr.Main.isWork;
import static com.igor2i.innopolis.lesson_works.lesson6.kr.Main.setShutdown;

/**
 * Created by igor2i on 13.02.17.
 */
public class CounterMap implements Runnable {

    private Queue<Integer> queue;

    private final Object lock;

    private HashMap<Integer, Integer> hashMapCounter = new HashMap<>();

    private static int takt = 0;

    public CounterMap(Queue<Integer> queue, Object lock) {
        this.queue = queue;
        this.lock = lock;
    }

    @Override
    public void run() {

        while (isWork()) {

            synchronized (lock){
                try {
                    lock.wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }

            if(queue.peek() != null) {

                int randomValue = queue.poll();

                if (!hashMapCounter.containsKey(randomValue)) {
                    hashMapCounter.put(randomValue, 1);
                } else {
                    hashMapCounter.replace(randomValue, hashMapCounter.get(randomValue) + 1);
                }


                if (++takt % 5 == 0) {
                    System.out.println("--------------------------------------");
                    hashMapCounter.forEach((randKey, countThisKey) ->
                    {

                        System.out.println("Число " + randKey + " повторялось " + countThisKey + " раз.");
                        if (countThisKey >= 5) {
                            setShutdown();
                        }
                    });

                }
            }


        }


    }


}
