package com.igor2i.innopolis.lesson_works.lesson6.kr;

import java.util.Queue;
import java.util.concurrent.LinkedBlockingQueue;

/**
 * Created by igor2i on 13.02.17.
 */
public class Main {

    private static boolean work = true;

    private static final Object lock = new Object();
    private static Queue<Integer> queue = new LinkedBlockingQueue<>();

    public static void main(String[] args) {

        Thread conterMap = new Thread(new CounterMap(queue,lock));
        Thread randomEvent = new Thread(new RandomEvent(queue,lock));

        conterMap.start();
        randomEvent.start();

        while (isWork()){

            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }


        }

    }

    public static boolean isWork(){
        return work;
    }

    public static void setShutdown(){
        Main.work = false;

        System.out.println("Good bye");
    }

}
