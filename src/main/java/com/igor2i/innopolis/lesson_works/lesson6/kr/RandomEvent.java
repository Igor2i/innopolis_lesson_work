package com.igor2i.innopolis.lesson_works.lesson6.kr;

import java.util.Queue;
import java.util.Random;

import static com.igor2i.innopolis.lesson_works.lesson6.kr.Main.isWork;

/**
 * Created by igor2i on 13.02.17.
 */
public class RandomEvent implements Runnable {

    private Queue<Integer> queue;

    private final Object lock;

    private Random random = new Random();

    public RandomEvent(Queue<Integer> queue, Object lock) {
        this.queue = queue;
        this.lock = lock;
    }

    @Override
    public void run() {

        while (isWork()){

            synchronized (lock) {

                lock.notifyAll();
            }
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }



            if(isWork()) {

                int randomInt = random.nextInt(99);

                queue.add(randomInt);
            }

        }


    }
}
